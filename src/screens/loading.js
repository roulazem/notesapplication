/* eslint-disable prettier/prettier */
import React from 'react';
import { ActivityIndicator, StyleSheet, View, Text } from 'react-native';
import { useTranslation } from 'react-i18next';
import AsyncStorage from '@react-native-async-storage/async-storage';

const settingOpts = (opts={lang: 'EN'}) => {
    const _settings = {
        lang: opts.lang
    };
    return _settings;
}

export const Loading = ({ dispatch, lang }) => {
    let componentState = -1;
    const [dot, setDot] = React.useState(0);

    React.useEffect(() => {
        // eslint-disable-next-line react-hooks/exhaustive-deps
        componentState = setTimeout(() => {
            //if dot < 3 add 1, otherwise mark one
            const _dot = dot < 3 ? (dot + 1) : 1;
            setDot(_dot);
        }, 1000);
        return () => clearTimeout(componentState);

    });

    const { t, i18n } = useTranslation();

    React.useEffect(() => {
        AsyncStorage.getItem('settings')
            .then((settingsJSON) => {
                const _settings = settingOpts(JSON.parse(settingsJSON));
                console.log({ _settings, settingsJSON });
                i18n.changeLanguage(_settings.lang)
                dispatch({ type: _settings.lang });
            })
            .catch(() => {
                const _settings = settingOpts();
                console.log({ _settings, settingsJSON });
                i18n.changeLanguage(_settings.lang);
                dispatch({ type: _settings.lang });
            });
    }, []);


    return (
        <View style = {styles.warpper}>
            <View style = {[styles.window, styles.wrapper]}>
                <ActivityIndicator size = "large" color ="#0000ff"/>
                <Text style = {styles.content}>{t("loading")}{'.'.repeat(dot)}</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    warpper: {
        width: '100%',
        justifyContent: 'center',
        flex: 1,
    },

    window: {
        backgroundColor: 'rgba(0,0,0,0.7)',
    },
    content: {
        padding: 8,
        fontSize: 24,
        fontWeight: 'bold',
        color: '#f1f1f1',
        justifyContent: 'center',
        textAlign: 'center',
    },
});

export default Loading;
