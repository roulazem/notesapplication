/* eslint-disable prettier/prettier */
import React from 'react';
import { View, Text } from 'react-native';
import { } from '../utils/myfs';
import { useTranslation } from 'react-i18next';
import AsyncStorage from '@react-native-async-storage/async-storage';

const settingOpts = (opts={lang: 'EN'}) => {
    const _settings = {
        lang: opts.lang
    };
    return _settings;
}

export const Dev  = ({count, dispatch, lang}) => {

    const { t, i18n } = useTranslation();

    React.useEffect(() => {
        AsyncStorage.getItem('settings')
            .then((settingsJSON) => {
                const _settings = settingOpts(JSON.parse(settingsJSON));
                console.log({ _settings, settingsJSON });
                i18n.changeLanguage(_settings.lang)
                dispatch({ type: _settings.lang });
            })
            .catch(() => {
                const _settings = settingOpts();
                console.log({ _settings, settingsJSON });
                i18n.changeLanguage(_settings.lang);
                dispatch({ type: _settings.lang });
            });
    }, []);

    return (<View>
        <Text>dev</Text>
        <Text>{t("counter")}: {count}</Text>
    </View>);
};
export default Dev;
