/* eslint-disable prettier/prettier */
import React from 'react';
import { View, ActivityIndicator, Button } from 'react-native';


export class TriggerLoading extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
        };
    }
    startLoading() {
        if (this.state.isLoading) {
            console.log('still loading');
        } else {
            this.setState({isLoading: true});
            setTimeout(()=> {
                this.setState({isLoading:false});
            }, 3000);
        }

    }

    render() {
        return (<View>
            <ActivityIndicator size="large" color="#f1f1f1"/>
            <Button
                disabled = {this.state.isLoading}
                title={"Trigger Loading"}
                onPress = {() => this.startLoading()}
            />
        </View>);
    }

}

export default TriggerLoading;
