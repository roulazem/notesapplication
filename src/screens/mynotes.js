import React from 'react';
import { View, Text, Button } from 'react-native';
import { listNotes } from '../utils/myfs';
import { useTranslation } from 'react-i18next';
import AsyncStorage from '@react-native-async-storage/async-storage';

const settingOpts = (opts={lang: 'EN'}) => {
    const _settings = {
        lang: opts.lang
    };
    return _settings;
}

export const MyNotes = ({ navigation, dispatch, lang }) => {

    const { t, i18n } = useTranslation();

    React.useEffect(() => {
        AsyncStorage.getItem('settings')
            .then((settingsJSON) => {
                const _settings = settingOpts(JSON.parse(settingsJSON));
                console.log({ _settings, settingsJSON });
                i18n.changeLanguage(_settings.lang)
                dispatch({ type: _settings.lang });
            })
            .catch(() => {
                const _settings = settingOpts();
                console.log({ _settings, settingsJSON });
                i18n.changeLanguage(_settings.lang);
                dispatch({ type: _settings.lang });
            });
    }, []);

    const [ notes, setNotes ] = React.useState([]);

    React.useEffect(() => {
        let isSubscribed = true;
        listNotes().then((_notes) => {
            console.log({_notes});
            if (isSubscribed) {
                setNotes(_notes)
            }
        });
        return () => isSubscribed = false;
    }, []);

    const openEditor = (note) => {
        console.log(`open note: ${note}`);
        navigation.navigate('note-editor', { filename: note });
    }

    

    return (<View>
        <Text>{t("mynotes")}</Text>
        {
            notes.map((note, i) => (<View key={`note-${i}`}>
                <Button title={note} onPress={() => openEditor(note)}></Button>
            </View>))
        }
    </View>);
}

export default MyNotes;