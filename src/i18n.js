import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';


const resources = {
    EN: {
        translation: {
            Finnish: "Finnish",
            English: "English",
            home: "Home",
            settings: "Settings",
            mynotes: "My Notes",
            note: "Note",
            createnote: "Create Note",
            updatenote: "Update note",
            titleplaceholder: "title",
            textplaceholder: "text here",
            save: "Save",
            buttons: {
                home: 'home',
                mynotes:'my notes',
                'note-editor':'note editor',
                'create-note':'new note',
                dev:'dev',
                loading:'loading',
                'trigger-loading':'trigger loading',
                settings:'settings',
                sensor: 'sensors'
            },
            increment: "Increment",
            decrement: "decrement",
            loading: "Loading",
            triggerloading: "Trigger loading",
            counter: "Count",
        }
    },
    FI: {
        translation: {
            Finnish: "Suomi",
            English: "Englanti",
            home: "Koti",
            settings: "Asetukset",
            mynotes: "Muistioni",
            note: "Muistio",
            createnote: "Uusi muistio",
            updatenote: "Muoka muistio",
            titleplaceholder: "otsikko",
            textplaceholder: "teksti tähän",
            save: "Tallenna",
            buttons: {
                home: 'koti',
                mynotes:'muistioni',
                'note-editor':'muoka muistio',
                'create-note':'uusi muistio',
                dev:'dev',
                loading:'lataus',
                'trigger-loading':'käynstä lataus',
                settings:'aseukset',
                sensor: 'anturit'
            },
            increment: "lisää",
            decrement: "vähennä",
            loading: "Lataus",
            triggerloading: "Käynistä lataus",
            counter: "Laskuri",
        }
    }
};

i18n
    .use(initReactI18next)
    .init({
        compatibilityJSON: 'v3',
        resources,
        lng: "EN",
        interpolation: {
            escapeValue: false
        }
});

